package com.celciusfahrenheit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.io.PrintWriter;

public class translator extends HttpServlet {

//    public void init() throws ServletException {
//        quoter = new Quoter();
//    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");

        Double celcius;
        try {
            celcius = Double.parseDouble(request.getParameter("celcius"));
        } catch (NumberFormatException e) {
            celcius = null;
        }


        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Book Quote";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celcius: " +
                request.getParameter("celcius") + "\n" +
                "  <P>Fahrenheit: " + (celcius == null ? "Could not parse celcius" : Calculation(celcius)) +
                "\n</BODY></HTML>");
    }

    public double Calculation(double celcius) {

        return celcius*1.8+32;

    }
}
